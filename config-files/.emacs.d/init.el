;; -*- mode: emacs-lisp -*-

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; If ~/.elisp/ exists, add it to load-path
(if (file-accessible-directory-p (expand-file-name "~/.elisp/"))
    (add-to-list 'load-path (expand-file-name "~/.elisp/")))


;; If ~/.emacs.d/libs/ exists, add it to load-path
(if (file-accessible-directory-p (expand-file-name "~/.emacs.d/libs/"))
    (add-to-list 'load-path (expand-file-name  "~/.emacs.d/libs/")))

;; If there are local customizations, load them
(if (file-readable-p (expand-file-name "~/.emacs-local.el"))
    (load-file (expand-file-name "~/.emacs-local.el")))

;; Lookup GTK+ symbols in devhelp documentation.
(autoload 'gtk-lookup-symbol "gtk-look" nil t)
(define-key global-map (kbd "C-h C-j") 'gtk-lookup-symbol)

;; Save customizations done through customization buffers to a separate file
(setq  custom-file  "~/.emacs-custom.el")
(if (file-readable-p (expand-file-name custom-file))
    (load-file (expand-file-name custom-file)))

;; Enable auto fill in text mode
(toggle-text-mode-auto-fill)

;; Better buffer name disambiguation
(require 'uniquify)

;; Loads custom theme. Requires emacs 24
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; Load the modus-vivendi theme. Don't ask for confirmation.
(load-theme 'modus-vivendi t nil)

;; Use smart-mode-line
(if (require 'smart-mode-line nil t)
    (setq sml/theme 'dark)
    (setq sml/no-confirm-load-theme t)
    (sml/setup))

;; Set default font to "Inconsolata"
;; Font can be downloaded from https://fonts.google.com/specimen/Inconsolata
(add-to-list 'default-frame-alist '(font . "Inconsolata"))
(set-face-attribute 'default t :font "Inconsolata")

;; Enable ibuffer
(require 'ibuffer)

;; Sort buffer by major-mode
(setq ibuffer-default-sorting-mode 'major-mode)

;; What buffers should ibuffer show?
(setq ibuffer-always-show-last-buffer t)
(setq ibuffer-view-ibuffer t)

;; Use ibuffer instead of the default buffer list
(global-set-key  (kbd "C-x C-b")  'ibuffer-other-window)

;; Show line and column numbers in the mode line
(setq line-number-mode t)
(setq column-number-mode t)

;; Show size of buffer in the mode line
(setq size-indication-mode t)

;; Display time and date in mode line
(setq display-time-day-and-date t)
(display-time)

;; highlight empty lines at end of file
(setq indicate-empty-lines t)

;; Don't add new lines when going to next line at end of file
(setq next-line-add-newlines nil)

;; highlight current line
(global-hl-line-mode t)

;; Highlight matching parenthesis
(show-paren-mode t)


;; Display line numbers on the left. If version is 26 or later, use
;; display-line-numbers-mode with relative line number. Otherwise, use
;; linum-mode.
(if (version<= "26.0.50" emacs-version)
    (progn
      (setq display-line-numbers-type 'relative)
      (global-display-line-numbers-mode))
  (progn
    (global-linum-mode t)
    (setq linum-format "%d ")))

;; Kill line contents and newline with one C-k
(setq kill-whole-line t)

;; Highlight trailing whitespaces ...
(setq show-trailing-whitespace t)

;; ... and delete them before saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Stop scrolling compilation output at the first error
(setq compilation-scroll-output 'first-error)

;; Automatically jump to the first error during compilation
(setq compilation-auto-jump-to-first-error t)

;; Always show syntactic analysis information when indenting lines in c-mode
(setq c-echo-syntactic-information-p t)

;; Warn about some syntactic errors while editing
(setq c-report-syntactic-errors t)

;; Tab key always indent in c-mode
(setq c-tab-always-indent t)

;; Show current function name in mode line
(setq which-function-mode t)

;; Add final newline when saving
(setq require-final-newline t)

;; Enable git version control mode
(require 'vc-git)

;; Allways follow symbolic links to version controlled files.
(setq vc-follow-symlinks t)

;; Keep changes for one file together when adding entries to a Changelog file.
(setq add-log-keep-changes-together t)

;; Full name and email for changelog entries.
(setq add-log-full-name "Goedson Teixeira Paixão")
(setq add-log-mailing-address "goedson@debian.org")

;; Automatically revert buffers after checkout
(global-auto-revert-mode)

;; Load magit, an interactive mode for manipulating Git repositories
(if (require 'magit nil t)
    (progn
      ;; Stop magit from showing warning about magit-auto-revert-mode
      (setq magit-last-seen-setup-instructions "1.4.0")

      ;; Load magit's stgit support
      ;; You will need magit 1.1.0 or later for this
      (if (require 'magit-stgit nil t)
	  (progn
	    (add-hook 'magit-mode-hook 'magit-stgit-mode)))

      ;; Bind magit status to C-x v b
      (global-set-key [(control x) (v) (b)] 'magit-status)))

;; Load stgit support
(require 'stgit nil t)

;; Load monky, aka  magit for mercurial
(require 'monky nil t)

;; Change default smerge-mode command prefix
(setq smerge-command-prefix "\C-cv")

;; Automatically enable smerge-mode in buffer with conflict markers
;; Copied from:
;; https://emacs.stackexchange.com/questions/16469/how-to-merge-git-conflicts-in-emacs
(defun my-enable-smerge-maybe ()
  (when (and buffer-file-name (vc-backend buffer-file-name))
    (save-excursion
      (goto-char (point-min))
      (when (re-search-forward "^<<<<<<< " nil t)
        (smerge-mode +1)))))

(add-hook 'find-file-hook #'my-enable-smerge-maybe)

;; Do not show startup screen
(setq inhibit-startup-screen t)

;; Enable narrow-to-region
(put 'narrow-to-region 'disabled nil)

;; Move through logical lines
(setq line-move-visual nil)

;; Navigation in compilation result buffers
(global-set-key [M-down]    #'next-error)
(global-set-key [M-up]      (lambda () (interactive) (next-error -1)))

;; Support for modern C++
(if (require 'modern-cpp-font-lock nil t)
    (progn
      (modern-c++-font-lock-global-mode t)))

;; Activate c-mode's minor modes
(defun my-cc-minor-modes ()
  (subword-mode 1)
  (c-toggle-electric-state 1)
  (c-toggle-auto-newline 1)
  (c-toggle-hungry-state 1)
  (c-toggle-syntactic-indentation 1))
(add-hook 'c-mode-common-hook 'my-cc-minor-modes)

;; Activate context sensitive line breaking in cc-mode
(defun my-cc-mode-key-bindings ()
  (local-set-key (kbd "RET") 'c-context-line-break)
  (local-set-key (kbd "C-o") 'c-context-open-line))
(add-hook 'c-mode-common-hook 'my-cc-mode-key-bindings)

;; Enable auto fill in cc-mode
(add-hook 'c-mode-common-hook 'auto-fill-mode)

;; C-mode formatting style
(setq c-default-style
      '((java-mode . "java")
        (awk-mode . "awk")
        (other . "gnu")))

;; Repeat C-SPC after C-u to pop mark repeatedly.
(setq set-mark-command-repeat-pop t)

;; Display battery status in modeline
(display-battery-mode 1)

(setq scroll-conservatively 100)

(setq scroll-error-top-bottom t)

;; CEDET config
(require 'cedet)
(require 'semantic)
(require 'srecode)
(global-ede-mode 1)                      ; Enable the Project management system
(global-srecode-minor-mode 1)            ; Enable template insertion menu

;; Enable on-the-fly syntax checking
(require 'flymake)
(add-hook 'find-file-hook 'flymake-find-file-hook)
(define-key flymake-mode-map (kbd "M-n") 'flymake-goto-next-error)
(define-key flymake-mode-map (kbd "M-p") 'flymake-goto-prev-error)

;; Code completion setup.
;; Enable company-mode everywhere.
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; Org-mode headers start unfolded
(setq org-startup-folded nil)
(setq org-startup-indented t)
(setq org-special-ctrl-a/e t)
(setq org-ctrl-k-protect-subtree t)
(setq org-insert-mode-line-in-empty-file t)
(setq org-catch-invisible-edits 'smart)
(setq org-enforce-todo-dependencies t)
(setq org-enforce-todo-checkbox-dependencies t)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

;; Global bindings for org mode
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c b") 'org-iswitchb)

;; Org-roam setup
(setq org-roam-directory "~/org-roam")

;; Basic setup for Org-roam dailies
(setq org-roam-dailies-directory "daily/")
(setq org-roam-dailies-capture-templates
      '(("d" "default" entry
         "* %?"
         :target (file+head "%<%Y-%m-%d>.org"
                            "#+title: %<%Y-%m-%d>\n"))))

(global-set-key (kbd "C-c d") 'org-roam-dailies-capture-today)
(global-set-key (kbd "C-c t") 'org-roam-dailies-find-today)
(global-set-key (kbd "C-c f") 'org-roam-node-find)
(global-set-key (kbd "C-c p") 'org-roam-dailies-find-previous-note)
(global-set-key (kbd "C-c n") 'org-roam-dailies-find-next-note)

(keymap-global-unset "C-z")

;;(add-hook 'after-init-hook 'org-roam-mode)
(org-roam-db-autosync-mode)

(winner-mode +1)
(define-key winner-mode-map (kbd "<M-left>") #'winner-undo)
(define-key winner-mode-map (kbd "<M-right>") #'winner-redo)

;; Show stack trace on error
;;(setq debug-on-error t)

;; Set search scroll behaviour
(setq scroll-margin 5)
(setq isearch-allow-scroll 1)

;; Insert paired parenthesis
(electric-pair-mode 1)

;; Support the use of dead-keys to enter accented Latin characters.
(require  'iso-transl)

(if (require 'ido nil t)
    (progn (ido-mode t)
	   (ido-everywhere)
	   (setq ido-ignore-extensions t)
	   (setq ido-use-filename-at-point 'guess)
	   (setq ido-enable-flex-matching t)
	   (setq ido-create-new-buffer 'always)
	   (add-hook 'ido-make-buffer-list-hook 'ido-summary-buffers-to-end)))

;; Projectile
(if (require 'projectile nil t)
    (progn
      ;; Enable projectile everywhere
      (projectile-global-mode)))

;; Enable rainbow delimiters, if present and set the face for unbalanced
;; parenthesis to stand out as errors.
;; Configuration adapted from tips in
;; http://timothypratley.blogspot.com.br/2015/07/seven-specialty-emacs-settings-with-big.html
(if (require 'rainbow-delimiters nil t)
    (progn
      ;; Activate rainbow delimiters in all programming modes.
      (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
      ;; Use moochrome rainbow delimiters ...
      (setq rainbow-delimiters-max-face-count 1)
      ;; ... and flag the unbalanced ones with the error face.
      (set-face-attribute 'rainbow-delimiters-unmatched-face nil
      			  :foreground 'unspecified
      			  :inherit 'error)
      ))

;; Activate hs-minor-mode in programming modes
(add-hook 'prog-mode-hook 'hs-minor-mode)

(require 'nov)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(require 'yasnippet)
(yas-global-mode 1)

;; Make this a server if we don't have one already
(server-start)
